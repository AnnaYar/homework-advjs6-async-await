/*Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

Асинхронність у JavaScript - це механізм виконання операцій, який дозволяє програмі продовжувати виконання коду, 
не чекаючи завершення певної операції. Це особливо важливо для операцій, які можуть займати час, 
таких як запити на сервер, читання файлів, таймаути та обробники подій.

У мові JavaScript асинхронність зазвичай досягається з використанням колбеків (callback functions), 
промісів (promises) та асинхронних функцій (async/await). Ці підходи дозволяють ефективно керувати потоком 
виконання коду, не блокуючи його під час тривалих операцій.


Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP 
адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.*/


const buttonIp = document.createElement('button');
buttonIp.textContent='Знайти по IP';
document.body.appendChild(buttonIp);
const ipInfo = document.createElement('div');
ipInfo.classList = 'ip-info';
document.body.appendChild(ipInfo);
const locationInfo = document.createElement('div');
locationInfo.classList = 'location';
document.body.appendChild(locationInfo);

buttonIp.addEventListener("click", () => {
    async function loadIp() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const ipData = await response.json();
        const userIp = ipData.ip
        console.log(userIp);
                
        ipInfo.textContent = `ІР адреса клієнта:${userIp}`;
        return userIp;

    }
    catch (error) {
        console.log(error, 'Помилка при отриманні IP');
    }
    }
loadIp().then(userIp => loadInfo(userIp));
});

async function loadInfo(userIp) {
    try {
        const response = await fetch(`http://ip-api.com/json/${userIp}?fields=continent,country,region,city,district`);
        const locationData = await response.json();
        console.log(locationData);

        locationInfo.innerHTML = `
        <p>Континент: ${locationData.continent}</p>
        <p>Країна: ${locationData.country}</p>
        <p>Регіон: ${locationData.region}</p>
        <p>Місто: ${locationData.city}</p>
        <p>Район: ${locationData.district}</p>`;
        return locationData;

    }
    catch (error) {
    console.log(error, 'Помилка при отриманні фізичної адреси');
    }
}




